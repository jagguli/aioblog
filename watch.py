#!/usr/bin/python3
"""
Script to watch files for changes and take actions


Usage:
    watch.py [options] [<path>]

Options:
    -h --help Show this 
    --js   watch js files
    --py   watch py files
"""
import os
import sys
import time
import logging
from docopt import docopt
from watchdog.observers import Observer
from watchdog.events import RegexMatchingEventHandler
from subprocess import check_output
from datetime import datetime

class MyRegexMatchingEventHandler(RegexMatchingEventHandler):
    def __init__(self, command, *args, **kwargs):
        super(MyRegexMatchingEventHandler, self).__init__(*args, **kwargs)
        self.command = command
        self.last_execution_time = datetime.now()

    def on_modified(self, event):
        if (datetime.now() - self.last_execution_time).seconds > 3:
            print(event.src_path, event.event_type)
            print(self.command)
            #check_output(command)
            os.system(" ".join(self.command))
            self.last_execution_time = datetime.now()

if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO,
                        format='%(asctime)s - %(message)s',
                        datefmt='%Y-%m-%d %H:%M:%S')
    arguments = docopt(__doc__, version='0.1')
    #print(arguments)
    path = arguments['<path>'] or "."
    js_command = ["node", "walkingdead.js"]
    py_command = "systemctl --user restart aioweb@blog".split()
    js_event_handler = MyRegexMatchingEventHandler(
        js_command, [r"[^.#].*\.js$", r"[^.#].*\.mustache$"], ignore_directories=True)
    py_event_handler = MyRegexMatchingEventHandler(
        py_command, [r"[^.#].*\.py"], ignore_directories=True)
    observer = Observer()
    if arguments['--py']:
        observer.schedule(py_event_handler, "src", recursive=True)
    if arguments['--js']:
        observer.schedule(js_event_handler, path, recursive=True)
    observer.start()
    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        observer.stop()
    observer.join()
