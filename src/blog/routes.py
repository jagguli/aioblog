from os.path import join, dirname, expanduser
from aioweb.static_handler import StaticFileHandler
from aioweb.router import Router
from .home_handler import HomeHandler, PostHandler


def get_routes(router=None):
    from aioweb.config import config
    if not router:
        router = Router()
    router.add_handler('/favicon.ico', StaticFileHandler,
                       dict(staticroot=config['default']['staticroot']))
    router.add_handler('/dojo/', StaticFileHandler,
                       dict(staticroot=config['default']['dojo'],
                            baseurl="/dojo/"))
    router.add_handler('/vendor/js/', StaticFileHandler,
                       dict(staticroot="./lib/js/", baseurl="/vendor/js/"))
    router.add_handler('/js/', StaticFileHandler,
                       dict(staticroot="./src/blog", baseurl="/js/"))
    router.add_handler('/jasmine/', StaticFileHandler,
                       dict(staticroot=config['default']['jasmine'],
                            baseurl='/jasmine/'))
    #router.add_handler('/intern/', StaticFileHandler,
    #                   dict(staticroot=config['default']['intern'],
    #                        baseurl='/intern/'))
    #router.add_handler('/mocha/', StaticFileHandler,
    #                   dict(staticroot=config['default']['mocha'],
    #                        baseurl='/mocha/'))

    app_routes = Router("/", (
        (r"/post/([^/]*)$", PostHandler),
        (r"/([^/]*)$", HomeHandler),
       # (r"/(supplementme)/(.*).js", StaticFileHandler, dict(
       #     staticroot=join(dirname(__file__), 'js'),
       #     baseurl="/supplementme/")),
       # (r"/(supplementme)/(.*).html", StaticFileHandler, dict(
       #     staticroot=join(dirname(__file__), 'html'),
       #     baseurl="/supplementme/")),
       # (r"/(supplementme)/(.*).css", StaticFileHandler, dict(
       #     staticroot=join(dirname(__file__), 'css'),
       #     baseurl="/supplementme/")),
       # (r"/auth/login$", AuthHandler),
       # (r"/meal/{0,1}(.*)$", MealHandler),
       # (r"/nutrients/{0,1}(.*)$", NutrientHandler),
       # (r"/food/{0,1}(.*)$", FoodHandler),
       # (r"/test/{0,1}(.*)$", WidgetTestHandler))
    ))
    router.add_handler('/', app_routes)
    return router
