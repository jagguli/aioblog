from aioweb.handler import Handler
from aioweb.renderers import JsonRenderer, HtmlRenderer
from .controller import HomeController
from .model import Post
import os


class ResourceHandler(Handler):
    renderer = JsonRenderer()

    def get(self, request_args=None):
        docs = self.controller.query(self.request)
        query = {'ok': True, 'query': self.query, 'result': docs}
        self.render(**query)
    
    def put(self, request_args=None):
        data = yield from self.get_json_data(True)
        controller = HomeController()
        schema = Post()
        post = schema.deserialize(data)
        r = yield from controller.new_post(post)
        query = {'ok': True, 'post': schema.serialize(r)}
        self.render(**query)
        
    def update(self, request_args=None):
        pass


class PostHandler(ResourceHandler):
    controller = HomeController()
    

class HomeHandler(Handler):
    renderer = HtmlRenderer([os.path.dirname(__file__)])
    def get(self, request_args=None):
        self.render("home")
