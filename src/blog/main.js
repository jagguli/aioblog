/*define([
    "dojo/has", // Dojo's feature detection module
    "require"   // The "context" require, that is aware of the current context
], function(has, require){
    var app = {};

    if(has("host-browser")){
        // we are running under a browser
        console.log("yay");
        require([ "dojo/request" ], function(request){
            console.log("yay");
            request.get("/helloworld.json", {
                handleAs: "json"
            }).then(function(data){
                console.log(data);
            });
        });
    }

    if(has("host-node")){
        // we are running under node
        require([ "dojo/node!fs" ], function(fs){
            var data = fs.readFileSync("helloworld.json");
            console.log(JSON.parse(data));
        });
    }

    return app;
});
*/
require([
    "dojo/_base/declare",
    "dojo/store/JsonRest",
    "dojo/store/Memory",
    "dojo/store/Cache",
    "dojo/store/Observable",
    "dojo/ready",
    "dojo/parser",
    "dojo/json",
    "dojo/when",
    "dojo/on",
    "dojo/dom-construct",
    "dojo/request",
    "dojo/domReady!"
], function(
    declare,
    JsonRest,
    Memory,
    Cache,
    Observable,
    ready,
    parser,
    json,
    when,
    on,
    domConstruct,
    request
){
    console.log("DOJOD");
    var postStore = new JsonRest({
        idProperty: '_id',
        target: "/post/"
    });
    var postMemoryStore = new Memory({ 
        idProperty: '_id',
    });

    var postStoreCache = new Cache(postMemoryStore, postStore);
    postStore.query({}).then(function(result){
        console.log(result);
    });
    postStoreCache.query({}).then(function(result){
        console.log(result);
    });
});
