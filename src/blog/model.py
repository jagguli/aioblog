import colander
from aioweb.db import Model

#class Post(Model):
#    required_fields = ['title', 'body', 'date']


class Post(Model):
    title = colander.SchemaNode(colander.String())
    datetime = colander.SchemaNode(colander.DateTime())
    body = colander.SchemaNode(colander.String())
