require(["dojo/request", "dojo/dom", "dojo/dom-construct", "dojo/json", "dojo/on", "dojo/domReady!"],
        function(request, dom, domConst, JSON, on){
            on(dom.byId("startButton"), "click", function(){
                domConst.place("<p>Requesting...</p>", "output");
                request("request/helloworld.json").then(function(text){
                    domConst.place("<p>response: <code>" + text + "</code>", "output");
                });
            });
        });
