import os
import imp
from aioweb.server import HttpServer
from aioweb.application import ProtocolFactory, startapp
from aioweb.config import set_config, configure_logging
from os.path import dirname
from blog.routes import get_routes
import logging
import asyncio


class BlogProtocolFactory(ProtocolFactory):
    def __call__(self):
        return self._call()

    def _call(self):
        try:
            import blog
            imp.reload(blog)
            router = get_routes()
            self.router = router
            
            return HttpServer(self.router, debug=True, keep_alive=75)
        except Exception as e:
            logging.exception("Failed to create HttpServer")


def main(loop):
    base_path = dirname(__file__)
    set_config(base_path, 'development')
    configure_logging(base_path)
    startapp(BlogProtocolFactory)

if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    main(loop)
