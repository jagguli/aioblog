from .controller import HomeController
from .home_handler import HomeHandler, PostHandler
from .model import Post
from loremipsum import generate_sentence, generate_paragraph
from datetime import datetime
from aioweb.db.mongodb_test import MongoDBTestCase
from aioweb.test import run_test_server as run_server
from .routes import get_routes
from aiohttp import client
import json
import os
from datetime import datetime
from loremipsum import generate_sentence, generate_paragraph


class PostHandlerTest(MongoDBTestCase):
    def setUp(self):
        self.base_path = os.path.dirname(__file__)
        super(PostHandlerTest, self).setUp()
        self.handler = PostHandler()

    def test_put_post(self):
        from .model import Post
        with run_server(self.loop, router=get_routes()) as httpd:
            url = httpd.url('/post/')
            meth = 'put'
            data = Post().serialize(
                {'title': 'some test title',
                 'datetime': datetime.now(),
                 'body': "some body"})
            data = json.dumps(data)
            r = self.loop.run_until_complete(
                client.request(meth, url, data=data, loop=self.loop))
            content1 = self.loop.run_until_complete(r.read())
            content = content1.decode()
            resp = json.loads(content)
        assert 'ok' in resp and resp['ok'] is True, resp
        assert 'post' in resp and '_id' in resp['post'], resp

    def test_get_post(self):
        with run_server(self.loop, router=get_routes()) as httpd:
            url = httpd.url('/post/')
            meth = 'get'
            params = {"title": "some", "match": "like"}
            r = self.loop.run_until_complete(
                client.request(meth, url, params=params, loop=self.loop))
            content1 = self.loop.run_until_complete(r.read())
            content = content1.decode()
            print(content)
            resp = json.loads(content)

        assert 'ok' in resp and resp['ok'] is True, resp

    def test_update_post(self):
        assert False
    def test_delete_post(self):
        assert False
